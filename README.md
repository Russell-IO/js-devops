# JS-DevOps
JS DevOps is a toolset for deploying a sample go application (in app directory)

### Preperation
You will need the following to use this toolset  
- AWS Secret & Access Keys  
- Ansible 1.9 installed & active  
- Keypair uploaded into eu-west-1 region called js-devops  

1: Clone the repository  
2: Copy the keypair into: `keys/js-devops` & chmod 0600 it  
3: Set up your ENV name `export DEVOPS_ENV=dev`  
4: Export your AWS Access Key `export AWS_ACCESS_KEY=${YOUR_ACCESS_KEY}`  
5: Export your AWS Secret Key `export AWS_SECRET_KEY=${YOUR_ACCESS_KEY}`  

### Environment Init
Run the Devops command:  
```
./devops init
```

### Deploy updated code to an existing environment
```
./devops deploy
```

### Demonstrate that traffic is being load balanced
```
./devops demo
```

